package com.example.checkscore

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import java.text.DecimalFormat

class MainActivity : AppCompatActivity() {

    private lateinit var etChScore: EditText
    private lateinit var ivChCheck: ImageView
    private lateinit var etEnScore: EditText
    private lateinit var ivEnCheck: ImageView
    private lateinit var etMathScore: EditText
    private lateinit var ivMathCheck: ImageView
    private lateinit var btCheck: Button
    private lateinit var tvTotal: TextView
    private lateinit var tvAve: TextView
    private lateinit var ivCheckAve: ImageView


    private var btCheckOnClick = View.OnClickListener { view ->
        var chScore = etChScore.text.toString().toDoubleOrNull()
        chScore?.let {
            ivChCheck.setImageResource( if (chScore >= 60) R.drawable.ic_pass else R.drawable.ic_fail)
        }
        var enScore = etEnScore.text.toString().toDoubleOrNull()
        enScore?.let {
            ivEnCheck.setImageResource( if (enScore >= 60) R.drawable.ic_pass else R.drawable.ic_fail)
        }
        var mathScore = etMathScore.text.toString().toDoubleOrNull()
        mathScore?.let {
            ivMathCheck.setImageResource( if (mathScore >= 60) R.drawable.ic_pass else R.drawable.ic_fail)
        }
        if (chScore != null && enScore != null && mathScore != null) {
            var total = chScore + enScore + mathScore
            tvTotal.text = total.toString()
            var ave = total/3
            var df = DecimalFormat("#")
            tvAve.text = df.format(ave)
            ivCheckAve.setImageResource(if (ave >= 60) R.drawable.ic_pass else R.drawable.ic_fail)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        etChScore = findViewById(R.id.etChScore)
        ivChCheck = findViewById(R.id.ivChCheck)
        etEnScore = findViewById(R.id.etEnScore)
        ivEnCheck = findViewById(R.id.ivEnCheck)
        etMathScore = findViewById(R.id.etMathScore)
        ivMathCheck = findViewById(R.id.ivMathCheck)
        btCheck = findViewById(R.id.btCheck)
        tvTotal = findViewById(R.id.tvTotal)
        tvAve = findViewById(R.id.tvAve)
        ivCheckAve = findViewById(R.id.ivCheckAve)

        btCheck.setOnClickListener(btCheckOnClick)
    }
}